package controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import com.google.zxing.WriterException;

import dataModel.DataModel;
import dataModel.IDataModelServices;
import exportCsvAccueil.Copier;
import exportPdf.AbstractBadgePDFExporter;
import exportPdf.badgeBenevole.BadgeBenevolePDFExporter;
import exportPdf.badgeInvite.BadgeInvitePDFExporter;
import importCsv.members.AbsractMemberCsvReader;
import importCsv.members.AssociationMemberCsvReader;
import importCsv.members.StructureMemberCsvReader;
import importCsv.teams.AbstractTeamCsvReader;
import importCsv.teams.AssociationCsvReader;
import importCsv.teams.StructureCsvReader;

public class Controller 
{
    private final IDataModelServices modelServices;

    public Controller(IDataModelServices pModelServices)
    {
        this.modelServices = pModelServices;
    }
        
	public static void main(String[] args) 
	{
		try
		{
	        DataModel model = new DataModel();
	        Controller controller = new Controller(model);
	        
	        //generation des badges fictifs
	        System.out.println("*********************GENERATION DES BENEVOLES FICTIFS*********************");
	        AbstractTeamCsvReader assoCsvReader = new AssociationCsvReader("resources/AssosDescriptions.csv", model);
	        AbsractMemberCsvReader assoMemberFictifCsvReader = new AssociationMemberCsvReader("resources/AssosMembresFictifs.csv", model);
	       
	        AbstractBadgePDFExporter assoFictifBadgePDFExporter = new BadgeBenevolePDFExporter(model, "generatedArtefacts/badgesBenevolesFictifs_"+AbstractBadgePDFExporter.BADGE_WIDTH_MM+"X"+AbstractBadgePDFExporter.BADGE_HEIGHT_MM+".pdf");
	        controller.generateBadges(assoCsvReader, assoMemberFictifCsvReader, assoFictifBadgePDFExporter);
	   
	        
	        //generation des badges benevoles
	        System.out.println("*********************GENERATION DES BENEVOLES*********************");
	        AbsractMemberCsvReader assoMemberCsvReader = new AssociationMemberCsvReader("resources/AssosMembres.csv", model);
	       
	        AbstractBadgePDFExporter assoBadgePDFExporter = new BadgeBenevolePDFExporter(model, "generatedArtefacts/badgesBenevoles_"+AbstractBadgePDFExporter.BADGE_WIDTH_MM+"X"+AbstractBadgePDFExporter.BADGE_HEIGHT_MM+".pdf");
	        controller.generateBadges(assoCsvReader.getImportedTeamNames(), assoMemberCsvReader, assoBadgePDFExporter);
	   
	        
	        
	        //generation des badges structures
	        System.out.println("*********************GENERATION DES STRUCTURES *********************\n");
	        AbstractTeamCsvReader structCsvReader = new StructureCsvReader("resources/StructuresDescriptions.csv", model);
	        AbsractMemberCsvReader structMemberCsvReader = new StructureMemberCsvReader("resources/StructuresMembres.csv", model);
	         
	        AbstractBadgePDFExporter structureBadgePDFExporter = new BadgeInvitePDFExporter(model, "generatedArtefacts/badgesPartenaires_"+AbstractBadgePDFExporter.BADGE_WIDTH_MM+"X"+AbstractBadgePDFExporter.BADGE_HEIGHT_MM+".pdf");
	        controller.generateBadges(structCsvReader, structMemberCsvReader, structureBadgePDFExporter);
	        
	        System.out.println("*********************GENERATION DES EXPORTS ACCUEIL BENEVOLE*********************");
	        //recopie des fichier benevoles et invites pour l'accueil, sans les accents ni les ' ni les /
	        Copier benevoleCopier = new Copier("resources/AssosMembres.csv", "generatedArtefacts/AssosMembres_Accueil.csv");
	        benevoleCopier.copy();
	        Copier inviteCopier = new Copier("resources/StructuresMembres.csv", "generatedArtefacts/StructuresMembres_Accueil.csv");
	        inviteCopier.copy();
		}
		catch (WriterException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

    public void generateBadges(AbstractTeamCsvReader pTeamCsvReader, AbsractMemberCsvReader pMemberCvReader, AbstractBadgePDFExporter pPDFExporter) throws FileNotFoundException, IOException, WriterException
    {
        pTeamCsvReader.importData();
        pMemberCvReader.importData();
        ArrayList<String> pTeamsToExport = pTeamCsvReader.getImportedTeamNames();
        pPDFExporter.export(pTeamsToExport);
    }
    
    public void generateBadges(ArrayList<String> pTeamsToExport, AbsractMemberCsvReader pMemberCvReader, AbstractBadgePDFExporter pPDFExporter) throws FileNotFoundException, IOException, WriterException
    {
        pMemberCvReader.importData();
        pPDFExporter.export(pTeamsToExport);
    }
}
