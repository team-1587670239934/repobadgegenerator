/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dataModel;

import java.io.IOException;

/**
 *
 * @author T0044185
 */
public class Association extends Team
{
    private final boolean isDefault;
    
    public Association(String pName, String pLogoFileName, boolean pIsDefault) throws IOException
    {
        super(pName, pLogoFileName);
        this.isDefault = pIsDefault;
    }   
    
    public boolean isDefault()
    {
        return this.isDefault;
    }
    
    @Override
    public boolean isEmptyPageRequested() 
    {
        return isDefault;
    }
}
