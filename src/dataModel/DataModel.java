package dataModel;

import java.io.IOException;
import java.util.ArrayList;

import exportPdf.StringNormalizer;

public class DataModel implements IDataModelServices
{
    private final ArrayList<Association> associations;
    private final ArrayList<Team> structures;

    public DataModel()
    {
        this.associations = new ArrayList<Association>();
        this.structures = new ArrayList<Team>();
    }
    
    @Override
    public void addMemberToTeam(String pId, String pFirstName, String pLastName, ArrayList<String> pMeals,
                    ArrayList<String> pLangues, String pTeamName, boolean pIsVege, boolean pIsLSF) throws IOException
    {
        Team team = this.getTeam(new StringNormalizer().normalize(pTeamName));
        if (team == null)
        {
            throw new IOException("L'�quipe "+pTeamName+" est inconnue");
        }
        
        
        ArrayList<ELangue> langues = new ArrayList<ELangue>();
        for (String langue:pLangues)
        {
                langues.add(ELangue.getValue(langue));
        }
        team.addMember(pId, pFirstName, pLastName, pMeals, langues, pIsVege, pIsLSF);
    }

	
    private Team getTeam(String pTeamName)
    {
        ArrayList<Team> teams = new ArrayList<Team>();
        teams.addAll(this.associations);
        teams.addAll(this.structures);
        Team res = null;
        for (Team team:teams)
        {
            if (team.getName().equals(pTeamName))
            {
                res = team;
            }
        }
        return res;
    }

    @Override
    public ArrayList<ELangue> getMemberLanguages(String pTeamName, int pIndex) 
    {
        return this.getTeam(pTeamName).getMemberLanguages(pIndex);
    }
    
    @Override
    public ArrayList<String>  getMemberMeals(String pTeamName, int pIndex) 
    {
        return this.getTeam(pTeamName).getMemberMeals(pIndex);
    }
    
    @Override
    public boolean getMemberIsVege(String pTeamName, int pIndex) 
    {
        return this.getTeam(pTeamName).getMemberIsVege(pIndex);
    }
    
    @Override
    public boolean getMemberIsLSF(String pTeamName, int pIndex) 
    {
        return this.getTeam(pTeamName).getMemberIsLSF(pIndex);
    }
    
    @Override
    public String getMemberId(String pTeamName, int pIndex) 
    {
        return this.getTeam(pTeamName).getMemberId(pIndex);
    }

    @Override
    public int getNbMembers(String pTeamName) 
    {
        return this.getTeam(pTeamName).getNbMembers();
    }

    @Override
    public String getMemberFirstName(String pTeamName, int pMemberIndex) 
    {
        return this.getTeam(pTeamName).getMemberFirstName(pMemberIndex);
    }
    
    @Override
    public String getMemberLastName(String pTeamName, int pMemberIndex) 
    {
        return this.getTeam(pTeamName).getMemberLastName(pMemberIndex);
    }

    @Override
    public ArrayList<String> getAssociationNames() 
    {
        ArrayList<String> names = new ArrayList<String>();
        for (Team asso:this.associations)
        {
            names.add(asso.getName());
        }
        return names;
    }
    
    @Override
    public void addAssociation(String pAssoName, String pAssoFileName, Boolean isDefaultAsso) throws IOException 
    {
        Association asso = new Association(pAssoName, pAssoFileName, isDefaultAsso);
        this.associations.add(asso);
    }

    @Override
    public String getTeamLogoFileName(String pTeamName) 
    {
        return this.getTeam(pTeamName).getLogoFileName();
    }

    @Override
    public String getDefaultAssociationName() 
    {
        String res="";
        for (int assoIndex=0; assoIndex<this.associations.size(); assoIndex++)
        {
            Association asso = this.associations.get(assoIndex);
            if (asso.isDefault())
            {
                res = asso.getName();
            }
        }
        return res;
    }

    @Override
    public boolean isEmptyPageRequested(String pTeamName) 
    {
        Team team = this.getTeam(pTeamName);
        return team.isEmptyPageRequested();
    }

    @Override
    public void addStructure(String pStructureName, String pStructureLogoFileName) throws IOException
    {
        this.structures.add(new Team(pStructureName, pStructureLogoFileName));
    }

    @Override
    public ArrayList<String> getStructureNames() 
    {
        ArrayList<String> names = new ArrayList<String>();
        for (Team editeur:this.structures)
        {
            names.add(editeur.getName());
        }
        return names;
    }
}