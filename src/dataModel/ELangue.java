package dataModel;

public enum ELangue 
{
	FRANCAIS("Francais"),
	ANGLAIS("Anglais"),
	ITALIEN("Italien"),
	ALLEMAND("Allemand"),
	ESPAGNOL("Espagnol"),;
	
	private final String label;
	
	ELangue(String pLabel)
	{
		this.label = pLabel;
	}
	
	public static ELangue getValue(String pLabel)
	{
		ELangue res = null;
		for (ELangue langue:ELangue.values())
		{
			if (langue.label.equals(pLabel))
			{
				res = langue;
			}
		}
		return res;
	}

}
