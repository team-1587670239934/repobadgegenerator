package dataModel;

import java.io.IOException;
import java.util.ArrayList;

public interface IDataModelServices {
	public void addMemberToTeam(String pId, String pFirstName, String pLastName, ArrayList<String> pMeals,
			ArrayList<String> langues, String pTeamName, boolean pIsVege, boolean pIsLSF) throws IOException;

	public void addAssociation(String pAssoName, String pAssoLogoFileName, Boolean isDefaultAsso) throws IOException;

	public void addStructure(String pStructureName, String pStructureLogoFileName) throws IOException;

	public ArrayList<ELangue> getMemberLanguages(String pTeamName, int pMemberIndex);

	public ArrayList<String>  getMemberMeals(String pTeamName, int pMemberIndex);

	public boolean getMemberIsVege(String pTeamName, int pMemberIndex);
	
	public boolean getMemberIsLSF(String pTeamName, int pMemberIndex);

	public String getMemberId(String pTeamName, int pMemberIndex);

	public int getNbMembers(String pTeamName);

	public String getMemberFirstName(String pTeamName, int pMemberIndex);

	public String getMemberLastName(String pTeamName, int pMemberIndex);

	public ArrayList<String> getAssociationNames();

	public ArrayList<String> getStructureNames();

	public String getTeamLogoFileName(String pTeamName);

	public String getDefaultAssociationName();

	public boolean isEmptyPageRequested(String pTeamName);
}
