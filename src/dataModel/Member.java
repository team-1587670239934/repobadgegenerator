package dataModel;

import java.util.ArrayList;

public class Member 
{
	private final String id;
	private final String firstName;
	private final String lastName;
	private final boolean isVege;
	private final boolean isLSF;
	private final ArrayList<String> meals;
	private final ArrayList<ELangue> langues;
	
	
	Member(String pId, String pFirstName, String pLastName, ArrayList<String> pMeals, ArrayList<ELangue> pLangues, boolean pIsVege, boolean pIsLSF)
	{
		this.id = pId;
		this.firstName = pFirstName;
		this.lastName = pLastName;
		this.meals = new ArrayList<String>(pMeals);
		this.langues = pLangues;
		this.isVege = pIsVege;
		this.isLSF = pIsLSF;
	}
	
	String getFirstName()
	{
		return this.firstName;
	}
	
	ArrayList<ELangue> getLangues()
	{
		return this.langues;
	}
        
    String getLastName()
    {
        return this.lastName;
    }
    
    ArrayList<String> getMeals()
    {
        return new ArrayList<String>(this.meals);
    }
    
    boolean isVege()
    {
    	return this.isVege;
    }
    
    boolean isLSF()
    {
    	return this.isLSF;
    }
    
    String getId()
    {
    	return this.id;
    }
}
