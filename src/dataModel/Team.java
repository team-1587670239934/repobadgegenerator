/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dataModel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author T0044185
 */
public class Team 
{
    final private String name;
    final private String logoFileName;
    private final ArrayList<Member> members = new ArrayList<Member>();
    
    
    public Team(String pName, String pLogoFileName) throws IOException
    {
        this.name = pName;
        this.logoFileName = pLogoFileName;
        
        File logoFile = new File("resources\\Logos\\"+this.logoFileName);
        if(!logoFile.isFile())
        {
        	throw new IOException("Impossible de cr�er l'�quipe "+this.name+" : Le logo "+this.logoFileName+" n'est pas disponible");
        }
    }
    
    public int getNbMembers() 
    {
	// TODO Auto-generated method stub
        return this.members.size();
    }
    
    public void addMember(String pId, String pFirstName, String pLastName, ArrayList<String> pMeals,
                    ArrayList<ELangue> pLangues, boolean pIsVege, boolean pIsLSF)
    {
        this.members.add(new Member(pId, pFirstName, pLastName, pMeals, pLangues, pIsVege, pIsLSF));
    }
            
    public String getMemberFirstName(int pIndex) 
    {
	return this.members.get(pIndex).getFirstName();
    }
    
    public String getMemberLastName(int pIndex) 
    {
	return this.members.get(pIndex).getLastName();
    }
    
    public ArrayList<ELangue> getMemberLanguages(int pIndex) 
    {
    	return this.members.get(pIndex).getLangues();
    }
    
    public ArrayList<String>  getMemberMeals(int pIndex) 
    {
    	return this.members.get(pIndex).getMeals();
    }
    
    public boolean getMemberIsVege(int pIndex) 
    {
    	return this.members.get(pIndex).isVege();
    }
    
    public boolean getMemberIsLSF(int pIndex) 
    {
    	return this.members.get(pIndex).isLSF();
    }
    
    public String getMemberId(int pIndex) 
    {
    	return this.members.get(pIndex).getId();
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public String getLogoFileName()
    {
        return this.logoFileName;
    }
    
    public boolean isEmptyPageRequested() 
    {
        return false;
    }
}
