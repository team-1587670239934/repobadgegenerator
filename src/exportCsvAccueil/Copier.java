package exportCsvAccueil;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import exportPdf.StringNormalizer;

public class Copier 
{
	final private String srcFileName;
	final private String targetFileName;
	
	public Copier(String pSrcFileName, String pTargetFileName)
	{
		this.srcFileName = pSrcFileName;
		this.targetFileName = pTargetFileName;
	}
	
	public void copy() throws FileNotFoundException, IOException
	{
		//flux de lecture du fichier src
    	InputStream srcFlux= new FileInputStream(this.srcFileName);
        InputStreamReader lecture= new InputStreamReader(srcFlux, StandardCharsets.UTF_8);
        BufferedReader br=new BufferedReader(lecture);    

        //flux de sortie du fichier target
        File targetFile = new File(this.targetFileName);
        //si le fichier existe on le supprime
        if (targetFile.exists()) 
        {
        	targetFile.delete();
        }
        targetFile.createNewFile();
        FileWriter targetFW = new FileWriter(targetFile.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(targetFW);
        
        //recopie du fichier source dans le fichier cible
        String currentLine = br.readLine();
        int nbLines = 0;
        while(currentLine!=null)
        {
        	String copiedLine = new StringNormalizer().normalize(currentLine);
        	copiedLine = copiedLine.replace("/", "-");
        	copiedLine = copiedLine.replace("'", " ");
        	bw.write(copiedLine+"\n");
        	
        	currentLine = br.readLine();
        	nbLines++;
        }	
        System.out.println(nbLines+" lignes copi�es");
        br.close();
        lecture.close();
        bw.close();
	}
}
