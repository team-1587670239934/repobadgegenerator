package exportPdf;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.regex.Pattern;

import com.google.zxing.WriterException;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.svg.converter.SvgConverter;

import dataModel.ELangue;
import dataModel.IDataModelServices;
import exportPdf.exportQrCode.QRCodeGenerator;


public abstract class AbstractBadgePDFExporter 
{

    final static int A4_PORTRAIT_WIDTH_MM = 210;
    final static int A4_PORTRAIT_HEIGH_MM = 297;
    final static String FONT_FILE = "FredokaOne-Regular.ttf";
    final static String FONT_FAMILY_NAME = "CustomFontFamilyName";
    
    
    /* tailles et poitionnement*/
    public final static int BADGE_WIDTH_MM = 93; 
    public final static int BADGE_HEIGHT_MM = 57;
    final protected static int NOMINAL_NAME_FONT_SIZE = 33;
    final protected static int QRCODE_MARGIN_MM = 5;
    final protected static int QR_CODE_SIZE_MM = 15;
    final protected static int SMALL_LINE_MAX_FONT_SIZE = 20;
    
    /*couleurs de qrcode*/
    public final static String VEGE_QR_CODE_COLOR = "00b400";
    public final static String OMNI_QR_CODE_COLOR = "ff8f00";
    public final static String SS_REPAS_QR_CODE_COLOR = "black";
    
    /*hauteur des lignes grises en base de badge, exprim�e en pourcentage de du QRCodeMargin*/
    public final static double BOTTOM_LINES_HEIGHT = 0.8;
    protected int nbExportedPage=1;
    
    final protected PdfFont FONT;
    final protected IDataModelServices dataModelServices;
    final protected PdfDocument pdfDoc;
    final protected Document doc;
    final private PdfWriter writer;   
    
	public AbstractBadgePDFExporter(IDataModelServices pDataModelServices, String pTargetFileName) throws IOException
	{
		FONT = PdfFontFactory.createFont(FONT_FILE);
		this.dataModelServices = pDataModelServices;
                //Initialize document
		//Initialize writer
		File pdfTargetFile = new File(pTargetFileName);
		
		//Initialize writer
		this.writer = new PdfWriter(pdfTargetFile);
                this.pdfDoc = new PdfDocument(writer);
		this.doc = new Document(pdfDoc);
	}
	
	
	
	public void export(ArrayList<String> pTeamsToExport) throws FileNotFoundException, IOException, WriterException
	{
        nbExportedPage=1;
            
		//calcul de la meilleure orientation
		int nbBadgesA4Portrait = this.getNbBadgesOnPage(PageSize.A4);	
		System.out.println("nbBadgesA4Portrait = "+nbBadgesA4Portrait);
		
		int nbBadgesA4Landscape = this.getNbBadgesOnPage(PageSize.A4.rotate());
		System.out.println("nbBadgesA4Landscape = "+nbBadgesA4Landscape);
		
		PageSize pageSize = nbBadgesA4Landscape > nbBadgesA4Portrait ? PageSize.A4.rotate():PageSize.A4;
		
		pdfDoc.setDefaultPageSize(pageSize);
	
		//generation du svg
		
		float pageHeightPts = pageSize.getHeight();
		float pageWidthPts = pageSize.getWidth();
		
		System.out.println("Page Width Pts = "+pageWidthPts);
		System.out.println("Page Height Pts = "+pageHeightPts);
		
		float badgeHeightPts = this.convertMMtoPts(BADGE_HEIGHT_MM);
		float badgeWidthPts = this.convertMMtoPts(BADGE_WIDTH_MM);
				
		System.out.println("Badge Width Pts = "+badgeWidthPts);
		System.out.println("Badge Height Pts = "+badgeHeightPts);
		
		int nbBadgesHorizontaux = getNbBadgesOnAxis(pageWidthPts, badgeWidthPts);
		int nbBadgesVerticaux = getNbBadgesOnAxis(pageHeightPts, badgeHeightPts);
		
		System.out.println("nbBadgesHorizontaux = "+nbBadgesHorizontaux);
		System.out.println("nbBadgesVerticaux = "+nbBadgesVerticaux);
		
		float svgHeightPts = nbBadgesVerticaux*badgeHeightPts;
		float svgWidthPts = nbBadgesHorizontaux*badgeWidthPts;
		
		System.out.println("svgHeightPts = "+svgHeightPts);
		System.out.println("svgWidthPts = "+svgWidthPts);
	
		
		float xMargin = (pageWidthPts - svgWidthPts)/2;
		float yMargin = (pageHeightPts - svgHeightPts)/2;
		
		this.exportTeams(pTeamsToExport, pageWidthPts, pageHeightPts, badgeWidthPts, badgeHeightPts, xMargin, yMargin, nbBadgesHorizontaux, nbBadgesVerticaux);
	

               
		
		//Close document
		this.doc.close();
		this.pdfDoc.close();
		this.writer.close();

	}
	
	private String generateMealLine(ExportedMember pExportedMember, int pMinMealIndex, int pMaxMealIndex)
	{
		String res;
		res = "";
		for (int mealIndex=pMinMealIndex; mealIndex<pMaxMealIndex; mealIndex++)
        {
        	res = res+"["+pExportedMember.getMeals().get(mealIndex)+"]"+" ";
        }
		res = res + "["+pExportedMember.getMeals().get(pMaxMealIndex)+"]";
		return res;
	}
	
	protected String generateMealsLines(ExportedMember pExportedMember, float badgeTopLeftXPts,float badgeTopLeftYPts, float badgeWidthPts, float badgeHeightPts)
	{
		String svgLines;
		String displayedMealsLine1;
		String displayedMealsLine2;
		int nbMeals = pExportedMember.getMeals().size();
        if (nbMeals > 0)
        {
        	if (nbMeals == 1)
        	{
        		//les repas ne sont que sur une ligne
        		displayedMealsLine1 = "";
        		displayedMealsLine2 = this.generateMealLine(pExportedMember, 0, 0);
        	}
        	else
        	{
        		//int nbMealsLine1 = (int)Math.ceil(nbMeals/2);
        		//displayedMealsLine1 = this.generateMealLine(pExportedMember, 0, nbMealsLine1-1);
        		displayedMealsLine1 = "";
        		int nbMealsLine1 = 0;
        		displayedMealsLine2 = this.generateMealLine(pExportedMember, nbMealsLine1, nbMeals-1);
        	}
        }
        else
        {
        	displayedMealsLine1 = "";
    		displayedMealsLine2 = "";
        }
        
        svgLines = this.generateSvgMealLine(displayedMealsLine1, badgeTopLeftXPts, badgeTopLeftYPts, badgeWidthPts, badgeHeightPts, 1);
        svgLines = svgLines + this.generateSvgMealLine(displayedMealsLine2, badgeTopLeftXPts, badgeTopLeftYPts, badgeWidthPts, badgeHeightPts, 0);
        return svgLines;
         
	}
	
	private String generateSvgMealLine(String pMealLine, float badgeTopLeftXPts,float badgeTopLeftYPts, float badgeWidthPts, float badgeHeightPts, int nbOffsets)
	{
		float qrCodeMarginPts = this.convertMMtoPts(QRCODE_MARGIN_MM);
        int smallLinesFontSize = new FontServices().adaptFontSizeForHeight(FONT, pMealLine, (float)BOTTOM_LINES_HEIGHT*qrCodeMarginPts, 1, SMALL_LINE_MAX_FONT_SIZE);
        
        //la hauteur de ligne est calcule sur la ligne en cours, mais est la m�me pour toute les lignes
        float lineHeight = new FontServices().getHeightPts(FONT, pMealLine, smallLinesFontSize);
        
        //float xSmallLine = badgeTopLeftXPts + badgeWidthPts/2;
        float mealLineWidthPts = new FontServices().getWidthPts(FONT, pMealLine, smallLinesFontSize);
        float xSmallLine = badgeTopLeftXPts + badgeWidthPts - qrCodeMarginPts - mealLineWidthPts/2;
        float ySmallLine = badgeTopLeftYPts + badgeHeightPts - (float)(qrCodeMarginPts/4) - lineHeight*nbOffsets;
        SvgServices svgServices = new SvgServices();
     

       
        return svgServices.generateText(xSmallLine, ySmallLine, smallLinesFontSize, pMealLine, "none", "gray"); 
	}
    
	protected String generateQRCode(ExportedMember pExportedMember, float badgeTopLeftXPts,float badgeTopLeftYPts, float badgeWidthPts, float badgeHeightPts) throws WriterException
	{
		//QR Code repas
        float qrCodeMarginPts = this.convertMMtoPts(QRCODE_MARGIN_MM);
        float qRCodeSizePts = this.convertMMtoPts(QR_CODE_SIZE_MM);
        float QrCodeRightBottomXPts = badgeTopLeftXPts + badgeWidthPts - qrCodeMarginPts;
        float QrCodeRightBottomYPts = badgeTopLeftYPts + badgeHeightPts - qrCodeMarginPts;
        String color = this.getQrCodeColor(pExportedMember);
        QRCodeGenerator generator  = new QRCodeGenerator(pExportedMember.getId(), color , QrCodeRightBottomXPts, QrCodeRightBottomYPts, qRCodeSizePts);
        return generator.generate();
	}
	
    private void exportTeams(ArrayList<String> pTeamsToExport, float pPageWidthPts, float pPageHeihtPts, float pBadgeWithPts, float pBadgeHeighPts, float pXMarginPts, float pYMarginPts, int pNbBadgesHorizontaux, int pNbBadgesVerticaux) throws IOException, WriterException
    {
        
        pTeamsToExport.sort(new StringComparator());
        //this.exportTeam(pTeamsToExport.get(0), pPageWidthPts, pPageHeihtPts, pBadgeWithPts, pBadgeHeighPts, pXMarginPts, pYMarginPts, pNbBadgesHorizontaux, pNbBadgesVerticaux);
        for (int teamIndex=0; teamIndex<pTeamsToExport.size(); teamIndex++)
        {
        	
        		this.exportTeam(pTeamsToExport.get(teamIndex), pPageWidthPts, pPageHeihtPts, pBadgeWithPts, pBadgeHeighPts, pXMarginPts, pYMarginPts, pNbBadgesHorizontaux, pNbBadgesVerticaux);
        

        }
        
    }
    
    private void exportTeam(String pTeamName, float pPageWidthPts, float pPageHeihtPts, float pBadgeWithPts, float pBadgeHeighPts, float pXMarginPts, float pYMarginPts, int pNbBadgesHorizontaux, int pNbBadgesVerticaux) throws IOException, WriterException
    {
        ArrayList<ExportedMember> membersToExport = new ArrayList<ExportedMember>();
        int nbMembers = this.dataModelServices.getNbMembers(pTeamName);
        for (int memberIndex=0; memberIndex<nbMembers; memberIndex++)
        {
        	String id = this.dataModelServices.getMemberId(pTeamName, memberIndex);
        	int nbDiffChars = this.computeMemberNbDiffChars(pTeamName, memberIndex);
            String firstName = this.dataModelServices.getMemberFirstName(pTeamName, memberIndex);
            String lastName = this.dataModelServices.getMemberLastName(pTeamName, memberIndex);
            ArrayList<String> meals = this.dataModelServices.getMemberMeals(pTeamName, memberIndex);
            boolean isVege = this.dataModelServices.getMemberIsVege(pTeamName, memberIndex);
            ArrayList<ELangue> langues = this.dataModelServices.getMemberLanguages(pTeamName, memberIndex);
            boolean isLSF = this.dataModelServices.getMemberIsLSF(pTeamName, memberIndex);
            membersToExport.add(new ExportedMember(id, firstName, lastName, nbDiffChars, isVege, meals, langues, isLSF));
        }
        membersToExport.sort(new ExportedMemberComparator());
        this.exportTeamMembers(pTeamName, membersToExport, pPageWidthPts, pPageHeihtPts, pBadgeWithPts, pBadgeHeighPts, pXMarginPts, pYMarginPts, pNbBadgesHorizontaux, pNbBadgesVerticaux);
    }
    
    protected abstract int computeMemberNbDiffChars(String pTeamName, int pIndexAnalysedMember);
        
	private void exportTeamMembers(String pTeamName, ArrayList<ExportedMember> pMembersToExport, float pPageWidthPts, float pPageHeihtPts, float pBadgeWithPts, float pBadgeHeighPts, float pXMarginPts, float pYMarginPts, int pNbBadgesHorizontaux, int pNbBadgesVerticaux) throws IOException, WriterException
	{
		final int nbMembersToExport = pMembersToExport.size();
                
		int nbExportedMembers = 0;
                
		while (nbExportedMembers < nbMembersToExport)
		{
			int nbRemainingMembers = nbMembersToExport - nbExportedMembers;
			int minIndex = nbExportedMembers;
			int nbMembersToExportOnPage = Math.min(nbRemainingMembers, pNbBadgesHorizontaux*pNbBadgesVerticaux);
			System.out.println("nbMembersToExportOnPage = "+nbMembersToExportOnPage);
                        ArrayList<ExportedMember> membersOnPage = new ArrayList<ExportedMember>(pMembersToExport.subList(minIndex, minIndex+nbMembersToExportOnPage));
                        
                        
			this.exportPage(pTeamName, membersOnPage ,pPageWidthPts, pPageHeihtPts, pBadgeWithPts, pBadgeHeighPts, pXMarginPts, pYMarginPts, pNbBadgesHorizontaux, pNbBadgesVerticaux);
			nbExportedMembers = nbExportedMembers + nbMembersToExportOnPage;
		}
        
		//ajout d'une page vide pour avoir des badges en rab avec le logo alchimie
        if (this.dataModelServices.isEmptyPageRequested(pTeamName))
        {
            this.exportPage(pTeamName,  new ArrayList<ExportedMember>(), pPageWidthPts, pPageHeihtPts, pBadgeWithPts, pBadgeHeighPts, pXMarginPts, pYMarginPts, pNbBadgesHorizontaux, pNbBadgesVerticaux);
        }

	}
	
	private void exportPage(String pTeamName, ArrayList<ExportedMember> pMembersToExportOnPage , float pPageWidthPts, float pPageHeihtPts, float pBadgeWithPts, float pBadgeHeighPts, float pXMarginPts, float pYMarginPts, int pNbBadgesHorizontaux, int pNbBadgesVerticaux) throws IOException, WriterException
	{
		this.pdfDoc.addNewPage();
        
		//en cas de page incomplète, finir avec des badges vides de l'association
        for (int memberIndex=pMembersToExportOnPage.size(); memberIndex < pNbBadgesHorizontaux*pNbBadgesVerticaux; memberIndex++)
        {
            pMembersToExportOnPage.add(new ExportedMember("QR Code sans data b�n�vole", "", "", 0, false, new ArrayList<String>(), new ArrayList<ELangue>(),false));
        }
		
        String svgCmd = "<svg  width=\""+(pPageWidthPts)+"pt\" height=\""+(pPageHeihtPts)+"pt\" xmlns=\"http://www.w3.org/2000/svg\">";
		
		svgCmd = svgCmd + "<defs><style type=\"text/css\">@font-face {font-family: "+FONT_FAMILY_NAME+";src: url('"+FONT_FILE+"');}</style></defs>";
		svgCmd = svgCmd+" <style><![CDATA[svg text{stroke:none}]]></style>";

        for (int exportedMemberIndex=0; exportedMemberIndex<pMembersToExportOnPage.size(); exportedMemberIndex++)
        {
            int badgeY = (int)Math.ceil(exportedMemberIndex/pNbBadgesHorizontaux);
            int badgeX = exportedMemberIndex - badgeY*pNbBadgesHorizontaux;
            float xTtopLeftRect = pXMarginPts+badgeX*pBadgeWithPts;
            float yTopLeftRect = pYMarginPts+badgeY*pBadgeHeighPts;
            svgCmd = svgCmd+ this.generateBadge(pTeamName, xTtopLeftRect, yTopLeftRect, pBadgeWithPts, pBadgeHeighPts, pMembersToExportOnPage.get(exportedMemberIndex));
        }
		
		svgCmd = svgCmd+"</svg>";
		//System.out.println(svgCmd);
                                
		SvgConverter.drawOnDocument(svgCmd, this.pdfDoc, nbExportedPage);
           
		nbExportedPage++;
                
                
	}
	
	protected abstract String generateBadge(String pTeamName, float pXPosPts, float pYPosPts, float pWidthPts, float pHeightPts, ExportedMember pExportedMember) throws MalformedURLException, WriterException, IOException;
	
	protected float convertMMtoPts(int mm)
	{
		return PageSize.A4.getHeight()*mm/A4_PORTRAIT_HEIGH_MM;
	}

	private int getNbBadgesOnAxis(float pageDimPts, float badgeDimPts)
	{
		int nbBadgesOnAxis = (int)Math.round(pageDimPts/badgeDimPts-0.5);
		return nbBadgesOnAxis;
	}
	
	protected String getQrCodeColor(ExportedMember pMember)
	{
		final String color;
		if (pMember.getMeals().size() == 0)
		{
			color = SS_REPAS_QR_CODE_COLOR;
		}
		else if (pMember.isVege())
		{
			color = VEGE_QR_CODE_COLOR;
		}
		else
		{
			color = OMNI_QR_CODE_COLOR;
		}
		return color;
	}

	private int getNbBadgesOnPage(PageSize pageSize)
	{
		final int nbBadges;
		
		
		float pageWidthPts = pageSize.getWidth();
		float pageHeightPts = pageSize.getHeight();
		
		float badgeWidthPts = this.convertMMtoPts(BADGE_WIDTH_MM);
		float badgeHeightPts = this.convertMMtoPts(BADGE_HEIGHT_MM);
		
		int nbBadgesOnXAxis = this.getNbBadgesOnAxis(pageWidthPts, badgeWidthPts);
		int nbBadgesOnYAxis = this.getNbBadgesOnAxis(pageHeightPts, badgeHeightPts);
		nbBadges = nbBadgesOnXAxis * nbBadgesOnYAxis;
		return nbBadges;
	}

}
