package exportPdf;

import dataModel.ELangue;
import java.util.ArrayList;

public class ExportedMember 
{
	private final String id;
	private final String firstName;
	//fullLastName est le nom de famille complet
	private final String lastName;
	//le short last name est utilis� pour le generateur n'indiquant que le pr�nom
	// nombre de caracteres differenciant dans le nom de famille.
	private final int nbDiffChars;
	private final boolean isVege;
	private final ArrayList<String> meals;
	private final ArrayList<ELangue> langues;
	private final boolean isLSF;
	
	
	ExportedMember(String pId, String pFirstName, String pLastName, int pNbDiffChars, boolean pIsVege, ArrayList<String> pMeals, ArrayList<ELangue> pLangues, boolean pIsLSF)
	{
		this.id = pId;
		this.firstName = pFirstName;
        this.lastName = pLastName;
        this.nbDiffChars = pNbDiffChars;
		this.langues = pLangues;
		this.meals = new ArrayList<String>(pMeals);
		this.isVege = pIsVege;
		this.isLSF = pIsLSF;
	}
	
	public String getFirstName()
	{
		return this.firstName;
	}
        
    public String getLastName()
	{
		return this.lastName;
	}
    
    public int getNbDiffChars()
	{
		return this.nbDiffChars;
	}
	
	public ArrayList<ELangue> getLangues()
	{
		return this.langues;
	}
	
	public boolean isVege()
	{
		return this.isVege;
	}
	
	public boolean isLF()
	{
		return this.isLSF;
	}
	
	public ArrayList<String> getMeals()
	{
		return new ArrayList<String>(this.meals);
	}
	
	public String getId()
	{
		return this.id;
	}
}
