/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package exportPdf;

import java.util.Comparator;

/**
 *
 * @author T0044185
 */
public class ExportedMemberComparator implements Comparator<ExportedMember>
{
    @Override
    public int compare(ExportedMember pMember1, ExportedMember pMember2)
    {
    	final int res;
    	StringNormalizer normalizer = new StringNormalizer();
    	
    	String firstName1 = normalizer.normalize(pMember1.getFirstName());
    	String firstName2 = normalizer.normalize(pMember2.getFirstName());
    	
    	if (!firstName1.equals(firstName2))
    	{
    		res = firstName1.compareTo(firstName2);
    	}
    	else
    	{
    		String lastName1 = normalizer.normalize(pMember1.getLastName());
        	String lastName2 = normalizer.normalize(pMember2.getLastName());
        	res = lastName1.compareTo(lastName2);
    	}
    	return res;
    }
    
}
