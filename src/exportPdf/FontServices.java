package exportPdf;

import com.itextpdf.kernel.font.PdfFont;

public class FontServices 
{
	//retourne la largeur de la chaine en pts
	public float getWidthPts(PdfFont pFont, String pText, int fontSize)
	{
		//le facteur 0.75 a �t� constat� empiriquement
		// ce service retourne une taille 25% trop longue...
		return (float)0.75*pFont.getWidth(pText, fontSize);
	}
	
	public float getHeightPts(PdfFont pFont, String text, int fontSize)
	{
		return pFont.getAscent(text, fontSize);
	}
	
	public int adaptFontSizeForWidth(PdfFont pFont, String pText, float pWidthConstraintPts, int pMinSize, int pMaxSize)
	{
		final int adaptedSize;
		if (this.getWidthPts(pFont, pText, pMaxSize)<pWidthConstraintPts)
		{
			adaptedSize = pMaxSize;
		}
		else if (pMinSize+1 == pMaxSize)
		{
			adaptedSize = pMinSize;
		}
		else
		{
			int testedSize = Math.round(pMinSize+pMaxSize)/2;
			if (this.getWidthPts(pFont, pText, testedSize)<pWidthConstraintPts)
			{
				adaptedSize = adaptFontSizeForWidth(pFont, pText, pWidthConstraintPts, testedSize, pMaxSize);
			}
			else
			{
				adaptedSize = adaptFontSizeForWidth(pFont, pText, pWidthConstraintPts, pMinSize, testedSize);
			}
		}
		return adaptedSize;
	}
	
	public int adaptFontSizeForHeight(PdfFont pFont, String pText, float pHeightConstraintPts, int pMinSize, int pMaxSize)
	{
		final int adaptedSize;
		if (this.getHeightPts(pFont, pText, pMaxSize)<pHeightConstraintPts)
		{
			adaptedSize = pMaxSize;
		}
		else if (pMinSize+1 == pMaxSize)
		{
			adaptedSize = pMinSize;
		}
		else
		{
			int testedSize = Math.round(pMinSize+pMaxSize)/2;
			if (this.getHeightPts(pFont, pText, testedSize)<pHeightConstraintPts)
			{
				adaptedSize = adaptFontSizeForHeight(pFont, pText, pHeightConstraintPts, testedSize, pMaxSize);
			}
			else
			{
				adaptedSize = adaptFontSizeForHeight(pFont, pText, pHeightConstraintPts, pMinSize, testedSize);
			}
		}
		return adaptedSize;
	}
	
}
