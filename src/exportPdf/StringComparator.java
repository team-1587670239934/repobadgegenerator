/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package exportPdf;

import java.util.Comparator;

/**
 *
 * @author T0044185
 */
public class StringComparator implements Comparator<String>
{
    @Override
    public int compare(String asso1, String asso2) 
    {
        return asso1.compareToIgnoreCase(asso2);
    }
}
