package exportPdf;

import java.text.Normalizer;
import java.util.regex.Pattern;

public class StringNormalizer {
	
	// suppression des accents
	public String normalize(String pInput) {
		String strTemp = Normalizer.normalize(pInput, Normalizer.Form.NFD);
		Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		return pattern.matcher(strTemp).replaceAll("");
	}
}
