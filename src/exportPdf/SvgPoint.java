package exportPdf;

public class SvgPoint 
{
	public final float x;
	public final float y;
	
	public SvgPoint(float pX, float pY)
	{
		this.x = pX;
		this.y = pY;
	}
}
