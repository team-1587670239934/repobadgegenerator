package exportPdf;

import java.util.ArrayList;

public class SvgPointList 
{
	private final float xOffset;
	private final float yOffset;
	private final float frameWidth;
	private final float frameHeight;
	final private ArrayList<SvgPoint> points = new ArrayList<SvgPoint>();
	
	public SvgPointList(float pXOffset, float pYOffset, float pFrameWidth, float pFrameHeight)
	{
		this.xOffset = pXOffset;
		this.yOffset = pYOffset;
		this.frameHeight = pFrameHeight;
		this.frameWidth = pFrameWidth;
		
	}
	
	
	
	
	public void addPoint(double pRelativeX, double pRelativeY)
	{
		this.points.add(new SvgPoint(this.xOffset + (float)pRelativeX*this.frameWidth, this.yOffset + (float)pRelativeY*this.frameHeight));
	}
	
	public int size()
	{
		return this.points.size();
	}
	
	public SvgPoint get(int pIndex)
	{
		return this.points.get(pIndex);
	}
	
	public void clear()
	{
		this.points.clear();
	}
}
