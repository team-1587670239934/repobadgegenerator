package exportPdf;

public class SvgServices 
{
	public SvgServices()
	{
		
	}
	
	public String generateRect(float pXPts, float pYPts, float pWidthPts, float pHeightPts, String pStrokeColor, String pFillColor)
	{
		final String svgLine;
		svgLine = "<rect x=\""+pXPts+"pt\" y=\""+pYPts+"pt\" width=\""+pWidthPts+"pt\" height=\""+pHeightPts+"pt\" style=\"stroke:"+pStrokeColor+";fill:"+pFillColor+";\"/>";
		return svgLine;
	}
	
	public String generateText(float pXPts, float pYPts, int pFontSize, String pText, String pStrokeColor, String pFillColor)
	{
		return "<text fill=\""+pFillColor+"\" stroke=\""+pStrokeColor+"\" x=\""+pXPts+"pt\" y=\""+pYPts+"pt\" font-family=\""+AbstractBadgePDFExporter.FONT_FAMILY_NAME+"\" font-size=\""+pFontSize+"\" dominant-baseline=\"middle\" text-anchor=\"middle\" >"+pText+"</text>\n";
	}
	
	public String generatePolygon(SvgPointList pPoints, String pStrokeColor, String pFillColor)
	{
		String svgPolygon = "<polygon stroke=\""+pStrokeColor+"\" fill=\""+pFillColor+"\" points=\"";
		for (int pointIndex=0; pointIndex<pPoints.size();pointIndex++)
		{
			svgPolygon = svgPolygon + pPoints.get(pointIndex).x + "pt," +pPoints.get(pointIndex).y+"pt";
			if (pointIndex != pPoints.size()-1)
			{
				svgPolygon = svgPolygon + ",";
			}
		}
		svgPolygon = svgPolygon + "\"/>";
		return svgPolygon;
	}
	
}
