/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package exportPdf.badgeBenevole;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import com.google.zxing.WriterException;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.layout.element.Image;

import dataModel.ELangue;
import dataModel.IDataModelServices;
import exportPdf.AbstractBadgePDFExporter;
import exportPdf.ExportedMember;
import exportPdf.FontServices;
import exportPdf.StringNormalizer;
import exportPdf.SvgServices;
import exportPdf.exportQrCode.QRCodeGenerator;

/**
 *
 * @author T0044185
 */
public class BadgeBenevolePDFExporter extends AbstractBadgePDFExporter
{
    
    final static int FLAG_WIDH_MM = 10;
    final static int FLAG_HEIGHT_MM = 5;
    final static int INTER_FLAGS_MM = 5;
    //les drapeaux sont centr�s (pas de XPOS pour les flags)
    final static double RELATIVE_YPOS_FLAGS = 0.75;
    //les prenoms sont centr�s (pas de XPOS pour les noms)
    final static double RELATIVE_YPOS_NAME = 0.5;
    
    
    //hauteur max de l'image du logo en pourcentage de la hauteur du badge
    final static double RELATIVE_ASSO_IMG_HEIGHT = 0.25;
    //largeur max de l'image du logo en pourcentage de la largeur du badge
    final static double RELATIVE_ASSO_IMG_WIDTH = 0.5;
    //eloigement de l'image asso depuis le bord du badge en MM
    final protected static int IMG_MARGINS_MM = QRCODE_MARGIN_MM;

    //hauteur max de l'image de fond en pourcentage de la hauteur du badge
    final static double RELATIVE_BKG_IMG_HEIGHT = 0.25;
    //largeur max de l'image de fond en pourcentage de la largeur du badge
    final static double RELATIVE_BKG_IMG_WIDTH = 0.5;
    //image de fond à appliquer
    final static String BKG_IMG_NAME =  "themeAlchimie.png";
    //image LSF à appliquer
    final static String LSF_IMG_NAME =  "lsf.jpg";
    
    public BadgeBenevolePDFExporter(IDataModelServices pDataModelServices, String pTargetFileName) throws IOException
    {
        super(pDataModelServices, pTargetFileName);
    }
    
    @Override
    public String generateBadge(String pTeamName, float pXPosPts, float pYPosPts, float pWidthPts, float pHeightPts, ExportedMember pExportedMember) throws MalformedURLException, WriterException, IOException
    {

        String firstName = pExportedMember.getFirstName(); 
        ArrayList<ELangue> langues = pExportedMember.getLangues();

        //bord du badge
        SvgServices svgGenerator = new SvgServices();
        String svgBadge = svgGenerator.generateRect(pXPosPts, pYPosPts, pWidthPts, pHeightPts, "black", "none");

        //image de fond du badge
        Image bgImage = this.buildSvgImage("resources/Logos/"+BKG_IMG_NAME, pXPosPts, pYPosPts, pWidthPts, pHeightPts, RELATIVE_BKG_IMG_WIDTH, RELATIVE_BKG_IMG_HEIGHT,ImgLocationE.TOP_RIGHT);
        doc.add(bgImage);
    	
        
        //prenom
        float xFirstName = pXPosPts + (float)(pWidthPts/2);
        float yFirstName = pYPosPts + (float)(pHeightPts*RELATIVE_YPOS_NAME);


        int fontSize = new FontServices().adaptFontSizeForWidth(FONT, firstName, pWidthPts, 1, NOMINAL_NAME_FONT_SIZE);

        svgBadge = svgBadge + svgGenerator.generateText(xFirstName, yFirstName, fontSize, firstName, "none", "black");


        //drapeaux

        SvgFlagDrawer flagDrawer = new SvgFlagDrawer();

        float flagWidthPts = this.convertMMtoPts(FLAG_WIDH_MM);
        float flagHeightPts = this.convertMMtoPts(FLAG_HEIGHT_MM);
        float interFlagsPts = this.convertMMtoPts(INTER_FLAGS_MM);

        int nbLangues = langues.size();
        float fullFlagsWidth = (nbLangues-1)*(flagWidthPts+interFlagsPts)+flagWidthPts;

        float xPosFlag1Pts = pXPosPts + pWidthPts/2 -fullFlagsWidth/2; 
        float yPosFagPts = pYPosPts + (float)(RELATIVE_YPOS_FLAGS*pHeightPts);

        for (int flagIndex=0; flagIndex<langues.size();flagIndex++)
        {
            try
            {
                svgBadge = svgBadge + flagDrawer.drawFlag(langues.get(flagIndex), xPosFlag1Pts+flagIndex*(flagWidthPts+interFlagsPts), yPosFagPts, flagWidthPts, flagHeightPts);
            }
            catch (NullPointerException e)
            {
                e.printStackTrace();
            }
        }
        
        
        
        //QR Code repas
        svgBadge = svgBadge + this.generateQRCode(pExportedMember, pXPosPts, pYPosPts, pWidthPts, pHeightPts);

        //nom an haut � droite du badge
        //les coordonn�es de la chaine sont le milieu de la chaine et le bas de la ligne. 
        
        if (!pExportedMember.getLastName().isEmpty())
        {
	        String firstPart = this.buildLastNameFirstClearPart(pExportedMember);
	        String secondPart = this.buildLastNameSecondHiddenPart(pExportedMember);
	        String thirdPart = this.buildLastNameThirdClearPart(pExportedMember);
	        
	        String displayLastName = firstPart+secondPart+thirdPart;
	        
	        float qrCodeMarginPts = this.convertMMtoPts(QRCODE_MARGIN_MM);
	        //ajout d'espace pour ajuster la hauteur
	        int lastNameFontSize = new FontServices().adaptFontSizeForHeight(FONT, " "+displayLastName+" ", (float)BOTTOM_LINES_HEIGHT*qrCodeMarginPts, 1, SMALL_LINE_MAX_FONT_SIZE);
	   
	        float lastNameWidth = new FontServices().getWidthPts(FONT, displayLastName, lastNameFontSize);
	       
	        float xLastName = pXPosPts + pWidthPts - qrCodeMarginPts - (float)lastNameWidth/2;
	        float yLastName = pYPosPts + qrCodeMarginPts;
	        
	        
	        final float actualXLastName;
	        if (xLastName + lastNameWidth/2 > pXPosPts+pWidthPts)
	        {
	        	actualXLastName = pXPosPts+pWidthPts - lastNameWidth/2;
	        }
	        else
	        {
	        	actualXLastName = xLastName;
	        }
	        
	        svgBadge = svgBadge + svgGenerator.generateText(actualXLastName, yLastName, lastNameFontSize, displayLastName, "none", "gray");
        }

        //les repas sous le badge
        svgBadge = svgBadge + this.generateMealsLines(pExportedMember, pXPosPts, pYPosPts, pWidthPts, pHeightPts);
        
        //logo association
        String assoLogFileName = this.dataModelServices.getTeamLogoFileName(pTeamName);
        Image assoImage = this.buildSvgImage("resources/Logos/"+assoLogFileName, pXPosPts, pYPosPts, pWidthPts, pHeightPts, RELATIVE_ASSO_IMG_WIDTH, RELATIVE_ASSO_IMG_HEIGHT, ImgLocationE.TOP_LEFT);
        doc.add(assoImage);
        
        //logo LSF
        if (pExportedMember.isLF())
        {
        	Image lsfImage = this.buildSvgImage("resources/Logos/"+LSF_IMG_NAME, pXPosPts, pYPosPts, pWidthPts, pHeightPts, RELATIVE_ASSO_IMG_WIDTH, RELATIVE_ASSO_IMG_HEIGHT, ImgLocationE.BOTTOM_LEFT);
        	doc.add(lsfImage);
        }
        
        
        return svgBadge;
    }
    
    private Image buildSvgImage(String pImgFilePath, float pXPosPts, float pYPosPts, float pWidthPts, float pHeightPts, double pMaxRelativeWidth, double pMaxRelativeHeight, ImgLocationE pImgLocation) throws MalformedURLException
    {
    	
        
    	ImageData data = ImageDataFactory.create(pImgFilePath);
        Image image = new Image(data);
        
        float maxAllocatedWidthPts =  pWidthPts*(float)pMaxRelativeWidth;
        float maxAllocatedHeightPts = pHeightPts*(float)pMaxRelativeHeight;
        
        double maxWidthRatio = maxAllocatedWidthPts/image.getImageWidth();
        double maxHeightRatio = maxAllocatedHeightPts/image.getImageHeight();
        
        float maxRatio = (float)Math.min(maxWidthRatio, maxHeightRatio);
        
        float scaledImgWidth = (float)image.getImageWidth()*maxRatio;
        float scaledImgHeight = (float)image.getImageHeight()*maxRatio;

        float marginToImgPts = this.convertMMtoPts(IMG_MARGINS_MM);
        
        final float xImgPos;
        final float yImgPos;
        if (pImgLocation == ImgLocationE.TOP_LEFT)
        {
        	xImgPos = pXPosPts + marginToImgPts;
        	yImgPos = this.pdfDoc.getDefaultPageSize().getHeight() - pYPosPts - marginToImgPts - scaledImgHeight;
        }
        else if (pImgLocation == ImgLocationE.TOP_RIGHT)
        {
        	//xImgPos = pXPosPts + pWidthPts/2 - scaledImgWidth/2;
        	xImgPos = pXPosPts + pWidthPts - scaledImgWidth - marginToImgPts;
        	yImgPos = this.pdfDoc.getDefaultPageSize().getHeight() - pYPosPts - marginToImgPts - scaledImgHeight;
        }
        else //BOTTOM_LEF
        {
        	xImgPos = pXPosPts + marginToImgPts;
        	yImgPos = this.pdfDoc.getDefaultPageSize().getHeight() - pYPosPts - pHeightPts + marginToImgPts;
        }
        	
        
        //La coordonn�e en Y de cette fonction part du bas de la feuille !
        //et le point indique est le coin inf�rieur gauche.
        image.setFixedPosition(this.nbExportedPage, xImgPos, yImgPos, scaledImgWidth);
        
        return image;
        
    }
    
   
    private String buildLastNameFirstClearPart(ExportedMember pExportedMember)
    {
    	String lastName = pExportedMember.getLastName();
    	int nbDiffChars = pExportedMember.getNbDiffChars();
    	return lastName.substring(0,nbDiffChars);
    }
    
    private String buildLastNameSecondHiddenPart(ExportedMember pExportedMember)
    {
    	final String res;
    	String lastName = pExportedMember.getLastName();
    	int nbDiffChars = pExportedMember.getNbDiffChars();
    	if (nbDiffChars == lastName.length())
    	{
    		res ="";
    	}
    	else if (nbDiffChars == lastName.length()-1)
    	{
    		res = "X";
    	}
    	else
    	{
    		int nbX = lastName.length() - nbDiffChars - 1;
    		res = new String(new char[nbX]).replace('\0', '*');
    	}
    	
    	return res;
    }
    
    private String buildLastNameThirdClearPart(ExportedMember pExportedMember)
    {
    	final String res;
    	String lastName = pExportedMember.getLastName();
    	int nbDiffChars = pExportedMember.getNbDiffChars();
    	if (nbDiffChars == lastName.length() || 
    			nbDiffChars == lastName.length()-1)
    	{
    		res ="";
    	}
    	else
    	{
    		res = lastName.substring(lastName.length()-1);
    	}
    	
    	return res;
    }
    
    @Override
    protected int computeMemberNbDiffChars(String pTeamName, int pIndexAnalysedMember)
    {
    	StringNormalizer normalizer = new StringNormalizer();
    	
    	String analysedFirstName = this.dataModelServices.getMemberFirstName(pTeamName, pIndexAnalysedMember);
    	String normalizedAnalysedFirstName = normalizer.normalize(analysedFirstName);
    	
    	String analysedLastName = this.dataModelServices.getMemberLastName(pTeamName, pIndexAnalysedMember);
    	
    	
    	int nbMembers = this.dataModelServices.getNbMembers(pTeamName);
    	int nbCommonChars = 0;
    	boolean ambiguityFound = false;
        for (int checkedMemberIndex=0; checkedMemberIndex<nbMembers; checkedMemberIndex++)
        {
        	if (checkedMemberIndex != pIndexAnalysedMember)
        	{
	        	String checkedFirstName = this.dataModelServices.getMemberFirstName(pTeamName, checkedMemberIndex);
	        	String normalizedCheckedFirstName = normalizer.normalize(checkedFirstName);
	        	if (normalizedAnalysedFirstName.equals(normalizedCheckedFirstName))
	        	{
	        		ambiguityFound = true;
	        		String checkedLastName = this.dataModelServices.getMemberLastName(pTeamName, checkedMemberIndex);
	        		int currentNbCommonChars = this.getNbCommonChars(analysedLastName, checkedLastName);
	        		nbCommonChars = currentNbCommonChars > nbCommonChars ? currentNbCommonChars : nbCommonChars;
	        	}
        	}
        }
        
        
        
        
        final int res;
        if (!ambiguityFound)
        {
        	res = 1;
        }
        else
        {
        	if (analysedLastName.length() == nbCommonChars)
        	{
        		res = nbCommonChars;
        	}
        	else
        	{
        		res = nbCommonChars+1;
        	}
        }
        
        return res;
        
    }
    
    private int getNbCommonChars(String pStr1, String pStr2)
    {
    	StringNormalizer normalizer = new StringNormalizer();
    	String normalizedStr1 = normalizer.normalize(pStr1);
    	String normalizedStr2 = normalizer.normalize(pStr2);
    	
    	int nbCommonChars = 0;
		boolean ambiguityRemains = true;
		while (ambiguityRemains && nbCommonChars <Math.min(normalizedStr1.length(),normalizedStr2.length() ))
		{
			nbCommonChars++;
			ambiguityRemains = normalizedStr1.substring(0,nbCommonChars).equals(normalizedStr2.substring(0,nbCommonChars));
		}
		
		final int res;
		if (!ambiguityRemains)
		{
			res = nbCommonChars-1;
		}
		else
		{
			res = nbCommonChars;
		}
		return res;
    }
    
}
