package exportPdf.badgeBenevole;

import dataModel.ELangue;
import exportPdf.SvgPointList;
import exportPdf.SvgServices;

public class SvgFlagDrawer {
	public String drawFlag(ELangue pCountry, float xPosPts, float yPosPts, float pWidthPts, float pHeightPts) {
		final String svgFlag;
		switch (pCountry) {
		case FRANCAIS:
			svgFlag = this.drawFrenchFlag(xPosPts, yPosPts, pWidthPts, pHeightPts);
			break;
		case ANGLAIS:
			svgFlag = this.drawBritishFlag(xPosPts, yPosPts, pWidthPts, pHeightPts);
			break;
		case ALLEMAND:
			svgFlag = this.drawGermanFlag(xPosPts, yPosPts, pWidthPts, pHeightPts);
			break;
		case ITALIEN:
			svgFlag = this.drawItalianFlag(xPosPts, yPosPts, pWidthPts, pHeightPts);
			break;
		case ESPAGNOL:
			svgFlag = this.drawSpannishFlag(xPosPts, yPosPts, pWidthPts, pHeightPts);
			;
			break;
		default:
			svgFlag = new SvgServices().generateRect(xPosPts, yPosPts, pWidthPts, pHeightPts, "black", "red");
			break;
		}
		return svgFlag;
	}

	private String drawFrenchFlag(float xPosPts, float yPosPts, float pWidthPts, float pHeightPts) {
		SvgServices svgServices = new SvgServices();

		String svgFlag = "";
		svgFlag = svgFlag + svgServices.generateRect(xPosPts, yPosPts, pWidthPts / 3, pHeightPts, "none", "blue");
		svgFlag = svgFlag + svgServices.generateRect(xPosPts + 2 * pWidthPts / 3, yPosPts, pWidthPts / 3, pHeightPts,
				"none", "red");
		svgFlag = svgFlag + svgServices.generateRect(xPosPts, yPosPts, pWidthPts, pHeightPts, "black", "none");
		return svgFlag;
	}

	private String drawGermanFlag(float xPosPts, float yPosPts, float pWidthPts, float pHeightPts) {
		SvgServices svgServices = new SvgServices();

		String svgFlag = "";
		svgFlag = svgFlag + svgServices.generateRect(xPosPts, yPosPts, pWidthPts, pHeightPts / 3, "none", "black");
		svgFlag = svgFlag
				+ svgServices.generateRect(xPosPts, yPosPts + pHeightPts / 3, pWidthPts, pHeightPts / 3, "none", "red");
		svgFlag = svgFlag + svgServices.generateRect(xPosPts, yPosPts + 2 * pHeightPts / 3, pWidthPts, pHeightPts / 3,
				"none", "yellow");
		svgFlag = svgFlag + svgServices.generateRect(xPosPts, yPosPts, pWidthPts, pHeightPts, "black", "none");
		return svgFlag;
	}

	private String drawSpannishFlag(float xPosPts, float yPosPts, float pWidthPts, float pHeightPts) {
		SvgServices svgServices = new SvgServices();

		String svgFlag = "";
		svgFlag = svgFlag + svgServices.generateRect(xPosPts, yPosPts, pWidthPts, pHeightPts / 4, "none", "red");
		svgFlag = svgFlag + svgServices.generateRect(xPosPts, yPosPts + pHeightPts / 4, pWidthPts, pHeightPts / 2,
				"none", "yellow");
		svgFlag = svgFlag + svgServices.generateRect(xPosPts, yPosPts + pHeightPts * 3 / 4, pWidthPts, pHeightPts / 4,
				"none", "red");
		svgFlag = svgFlag + svgServices.generateRect(xPosPts, yPosPts, pWidthPts, pHeightPts, "black", "none");
		return svgFlag;
	}

	private String drawItalianFlag(float xPosPts, float yPosPts, float pWidthPts, float pHeightPts) {
		SvgServices svgServices = new SvgServices();

		String svgFlag = "";
		svgFlag = svgFlag + svgServices.generateRect(xPosPts, yPosPts, pWidthPts / 3, pHeightPts, "none", "green");
		svgFlag = svgFlag + svgServices.generateRect(xPosPts + 2 * pWidthPts / 3, yPosPts, pWidthPts / 3, pHeightPts,
				"none", "red");
		svgFlag = svgFlag + svgServices.generateRect(xPosPts, yPosPts, pWidthPts, pHeightPts, "black", "none");
		return svgFlag;
	}

	private String drawBritishFlag(float xPosPts, float yPosPts, float pWidthPts, float pHeightPts) {
		String svgFlag = "";
		SvgServices svgServices = new SvgServices();

		/* coin superieur gauche */
		SvgPointList points = new SvgPointList(xPosPts, yPosPts, pWidthPts, pHeightPts);

		points.addPoint(0.1187, 0);
		points.addPoint(0.4000, 0);
		points.addPoint(0.4000, 0.2813);
		svgFlag = svgFlag + svgServices.generatePolygon(points, "none", "blue");

		points.clear();
		points.addPoint(0, 0);
		points.addPoint(0, 0.0833);
		points.addPoint(0.2502, 0.3333);
		points.addPoint(0.3333, 0.3333);
		svgFlag = svgFlag + svgServices.generatePolygon(points, "none", "blue");

		points.clear();
		points.addPoint(0, 0.125);
		points.addPoint(0, 0.3333);
		points.addPoint(0.2078, 0.3333);
		svgFlag = svgFlag + svgServices.generatePolygon(points, "none", "blue");

		/* coin superieur droit */

		points.clear();
		points.addPoint(0.6000, 0);
		points.addPoint(0.8813, 0);
		points.addPoint(0.6000, 0.2813);
		svgFlag = svgFlag + svgServices.generatePolygon(points, "none", "blue");

		points.clear();
		points.addPoint(0.9250, 0);
		points.addPoint(1, 0);
		points.addPoint(0.6666, 0.3333);
		points.addPoint(0.6000, 0.3333);
		points.addPoint(0.6000, 0.3250);
		svgFlag = svgFlag + svgServices.generatePolygon(points, "none", "blue");

		points.clear();
		points.addPoint(0.7917, 0.3333);
		points.addPoint(1, 0.3333);
		points.addPoint(1, 0.1250);
		svgFlag = svgFlag + svgServices.generatePolygon(points, "none", "blue");

		/* coin inferieur droit */

		points.clear();
		points.addPoint(0.6000, 1);
		points.addPoint(0.8813, 1);
		points.addPoint(0.6000, 0.7187);
		svgFlag = svgFlag + svgServices.generatePolygon(points, "none", "blue");

		points.clear();
		points.addPoint(1, 1);
		points.addPoint(1, 0.9167);
		points.addPoint(0.7498, 0.6666);
		points.addPoint(0.6666, 0.6666);
		svgFlag = svgFlag + svgServices.generatePolygon(points, "none", "blue");

		points.clear();
		points.addPoint(0.7917, 0.6666);
		points.addPoint(1, 0.6666);
		points.addPoint(1, 0.8750);
		svgFlag = svgFlag + svgServices.generatePolygon(points, "none", "blue");

		/* coin infeieur guauche */

		points.clear();
		points.addPoint(0.4000, 1);
		points.addPoint(0.1187, 1);
		points.addPoint(0.4000, 0.7187);
		svgFlag = svgFlag + svgServices.generatePolygon(points, "none", "blue");

		points.clear();
		points.addPoint(0.0750, 1);
		points.addPoint(0, 1);
		points.addPoint(0.3333, 0.6666);
		points.addPoint(0.4000, 0.6666);
		points.addPoint(0.4000, 0.6750);
		svgFlag = svgFlag + svgServices.generatePolygon(points, "none", "blue");

		points.clear();
		points.addPoint(0.2083, 0.6666);
		points.addPoint(0, 0.6666);
		points.addPoint(0, 0.8750);
		svgFlag = svgFlag + svgServices.generatePolygon(points, "none", "blue");

		points.clear();
		points.addPoint(0, 0.3958);
		points.addPoint(0.4375, 0.3958);
		points.addPoint(0.4375, 0);
		points.addPoint(0.5625, 0);
		points.addPoint(0.5625, 0.3958);
		points.addPoint(1, 0.3958);
		points.addPoint(1, 0.6042);
		points.addPoint(0.5625, 0.6042);
		points.addPoint(0.5625, 1);
		points.addPoint(0.4375, 1);
		points.addPoint(0.4375, 0.6042);
		points.addPoint(0, 0.6042);
		svgFlag = svgFlag + svgServices.generatePolygon(points, "none", "red");

		svgFlag = svgFlag + svgServices.generateRect(xPosPts, yPosPts, pWidthPts, pHeightPts, "black", "none");

		return svgFlag;

	}
}
