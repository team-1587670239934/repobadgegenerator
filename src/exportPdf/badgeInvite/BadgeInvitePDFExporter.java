/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package exportPdf.badgeInvite;


import java.io.IOException;
import java.net.MalformedURLException;

import com.google.zxing.WriterException;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.layout.element.Image;

import dataModel.IDataModelServices;
import exportPdf.AbstractBadgePDFExporter;
import exportPdf.ExportedMember;
import exportPdf.FontServices;
import exportPdf.SvgServices;

/**
 *
 * @author T0044185
 */
public class BadgeInvitePDFExporter extends AbstractBadgePDFExporter
{
	//largeur maximale des noms / prenoms en pourcentage du badge
	final static double RELATIVE_NAMES_WIDTH = 1;
    //eloigement de l'image depuis le bord du badge en MM
    final protected static int STRUCT_IMG_MARGIN_MM = QRCODE_MARGIN_MM;
    //positionnement des noms et prenoms en X
    //final static double RELATIVE_XPOS_NAME = (1-RELATIVE_STRUCT_RESERVED_WIDTH)/2+RELATIVE_STRUCT_RESERVED_WIDTH;
    final static double RELATIVE_XPOS_NAME = 0.5;
    //positionnement des noms et prenoms en Y
    final static double RELATIVE_YPOS_FIRST_NAME = 0.3;
    final static double RELATIVE_YPOS_LAST_NAME = 0.6;
    final static double RELATIVE_YPOS_SINGLE_NAME = 0.4;
    
    
    //hauteur max de l'image de la structure en pourcentage de la hauteur du badge
    final static double RELATIVE_STRUCT_IMG_HEIGHT = 0.25;
    //largeur max de l'image du logo en pourcentage de la largeur du badge
    final static double RELATIVE_STRUCT_IMG_WIDTH = 0.5;
    
    
    public BadgeInvitePDFExporter(IDataModelServices pDataModelServices, String pTargetFileName) throws IOException
    {
        super(pDataModelServices, pTargetFileName);
    }

    
    @Override
    protected String generateBadge(String pTeamName, float pXPosPts, float pYPosPts, float pWidthPts, float pHeightPts, ExportedMember pExportedMember) throws MalformedURLException, WriterException 
    {
        String firstName = pExportedMember.getFirstName(); 
        String lastName = pExportedMember.getLastName();
        
        
        SvgServices svgGenerator = new SvgServices();
        String svgBadge = svgGenerator.generateRect(pXPosPts, pYPosPts, pWidthPts, pHeightPts, "black", "none");

        
        
        //ecrit le nom et le pr�nom
        float xNames = pXPosPts + (float)(pWidthPts*RELATIVE_XPOS_NAME);
        final float yFirstName;
        if (lastName.isEmpty())
        {
        	yFirstName = pYPosPts + (float)(pHeightPts*RELATIVE_YPOS_SINGLE_NAME);
        }
        else
        {
        	yFirstName = pYPosPts + (float)(pHeightPts*RELATIVE_YPOS_FIRST_NAME);
        }
        float yLastName = pYPosPts + (float)(pHeightPts*RELATIVE_YPOS_LAST_NAME);

        float allocatedWidthForName = (float)pWidthPts*(float)(RELATIVE_NAMES_WIDTH);
        int firstNameFontSize = new FontServices().adaptFontSizeForWidth(FONT, firstName, allocatedWidthForName, 1, NOMINAL_NAME_FONT_SIZE);
        int lastNameFontSize = new FontServices().adaptFontSizeForWidth(FONT, lastName, allocatedWidthForName, 1, NOMINAL_NAME_FONT_SIZE);

        int fontSize = Math.min(firstNameFontSize, lastNameFontSize);
        svgBadge = svgBadge + svgGenerator.generateText(xNames, yFirstName, fontSize, firstName, "none", "black");
        svgBadge = svgBadge + svgGenerator.generateText(xNames, yLastName, fontSize, lastName, "none", "black");

        //ajoute le le logo
        String structureLogoFileName = this.dataModelServices.getTeamLogoFileName(pTeamName);
        if (!structureLogoFileName.equals(""))
        {
            System.out.println("resources/Logos/"+structureLogoFileName);
            ImageData data = ImageDataFactory.create("resources/Logos/"+structureLogoFileName);
            Image image = new Image(data);
            
            
            float maxAllocatedWidthPts =  pWidthPts*(float)RELATIVE_STRUCT_IMG_WIDTH;
            float maxAllocatedHeightPts = pHeightPts*(float)RELATIVE_STRUCT_IMG_HEIGHT;
            
            double maxWidthRatio = maxAllocatedWidthPts/image.getImageWidth();
            double maxHeightRatio = maxAllocatedHeightPts/image.getImageHeight();
            
            float maxRatio = (float)Math.min(maxWidthRatio, maxHeightRatio);
            
            float scaledImgWidth = (float)image.getImageWidth()*maxRatio;
            
            float marginToAssoImgPts = this.convertMMtoPts(STRUCT_IMG_MARGIN_MM);
            
            float xImgPos = pXPosPts + marginToAssoImgPts;
            float yImgPos = this.pdfDoc.getDefaultPageSize().getHeight() - pYPosPts - pHeightPts + marginToAssoImgPts;
      
            //La coordonn�e en Y de cette fonction part du bas de la feuille !
            //et la position est le coin inf�rieur gauche !
            image.setFixedPosition(this.nbExportedPage, xImgPos, yImgPos, scaledImgWidth);

            doc.add(image);
        }
        
        //ajoute le QR Code repas
        svgBadge = svgBadge + this.generateQRCode(pExportedMember, pXPosPts, pYPosPts, pWidthPts, pHeightPts);
        
        //les repas sous le badge
        svgBadge = svgBadge + this.generateMealsLines(pExportedMember, pXPosPts, pYPosPts, pWidthPts, pHeightPts);

        return svgBadge;
    }


	@Override
	protected int computeMemberNbDiffChars(String pTeamName, int pIndexAnalysedMember) {
		// TODO Auto-generated method stub
		return 0;
	}
}
