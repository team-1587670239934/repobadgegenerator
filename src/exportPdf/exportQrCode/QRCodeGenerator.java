package exportPdf.exportQrCode;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import exportPdf.SvgServices;

public class QRCodeGenerator
{
	private boolean[][] squareMaps;
	final private String content;
	final private String QRCodeColor;
	final private float QRCodeRightBottomXPts;
	final private float QRCodeRightBottomYPts;
	final private float QRCodeSizePts;
	
	final static int REQUESTED_SIZE = 200;
	
	public QRCodeGenerator(String pContent, String pQRCodeColor, float pQrCodeRightBottomXPts, float pQrCodeRightBottomYPts, float pQrCodeSizePts) 
	{
		this.content = pContent;
		this.QRCodeColor = pQRCodeColor;
		this.QRCodeSizePts = pQrCodeSizePts;
		this.QRCodeRightBottomXPts = pQrCodeRightBottomXPts;
		this.QRCodeRightBottomYPts = pQrCodeRightBottomYPts;
	}
	
	
	public BitMatrix generateMatrix(String data, int size) throws WriterException
	{
		BitMatrix bitMatrix = new QRCodeWriter().encode(data, BarcodeFormat.QR_CODE, size, size);
		return bitMatrix;
	}
	
	public String generate() throws WriterException
	{
		final String svg; 
		
		//g�n�ration de la matrice REQUESTED_SIZE bit
    	BitMatrix bitMatrix = this.generateMatrix(this.content, REQUESTED_SIZE);
    	
    	//etant donn� que la taille du qr code ne peut pas valoir exactement REQUESTED_SIZE, on recherche la taille exacte generee
    	//int actualTopLeftX = bitMatrix.getTopLeftOnBit()[0];
    	//int actualTopLeftY = bitMatrix.getTopLeftOnBit()[1];
    	//int actualBottomRightX = bitMatrix.getBottomRightOnBit()[0];
    	
    	//int actualSizeInPixel = bitMatrix.getEnclosingRectangle()[0];
    	
    	
    	int actualX = bitMatrix.getEnclosingRectangle()[0];
    	int actualY = bitMatrix.getEnclosingRectangle()[1];
    	
    	//calcul de la largeur d'un mini carre dans la matric generee
    	int squareSizeInPixel = 0;
    	while (bitMatrix.get(actualX+squareSizeInPixel, actualY+squareSizeInPixel) == true)
    	{
    		squareSizeInPixel++;
    	}
    	int bitMatrixSquareSizeInPixel = squareSizeInPixel;
    	
    	int actualSizeInPixel = bitMatrix.getEnclosingRectangle()[2]+1;
    	int bitMatrixSizeInSquares = actualSizeInPixel / bitMatrixSquareSizeInPixel;
    	
    	//System.out.println("Taille de chaque petit carr�e = "+bitMatrixSquareSizeInPixel);
    	//System.out.println("Taille du QR code en carr�s = "+bitMatrixSizeInSquares);

    	//constitution d'une matrix interm�diaire de boolean de la taille du nombre de carr� dans le QRcode.
    	buildSquareMap(bitMatrix, actualX, actualY, bitMatrixSquareSizeInPixel, bitMatrixSizeInSquares);
    	
    	//dessin SVG du QRcode � la taille souhaitee
    	svg = buildQRCode(bitMatrixSizeInSquares);
		
		return svg;
	}
	
	
	
	private void buildSquareMap(BitMatrix pBitMatrix,int actualX,int actualY,int pSquareSizeInPixels, int pMatrixSizeInSquares)
	{
	
		this.squareMaps = new boolean[pMatrixSizeInSquares][pMatrixSizeInSquares];
		for (int squareX=0; squareX<pMatrixSizeInSquares; squareX++)
		{
			for (int squareY=0; squareY<pMatrixSizeInSquares; squareY++)
			{
				this.squareMaps[squareX][squareY] = pBitMatrix.get(actualX+squareX*pSquareSizeInPixels, actualY+squareY*pSquareSizeInPixels);
			}
		}
		
	}
	
	private String buildQRCode(int pSizeInSquares)
	{
		String svg = "";
		float squareSizePts = this.QRCodeSizePts / pSizeInSquares;
	        
		SvgServices svgServices = new SvgServices();

        for (int x=pSizeInSquares-1; x>=0; x--)
        {
        	for (int y=pSizeInSquares-1; y>=0; y--)
      	  	{
        		if (this.squareMaps[x][y])
      		  	{
        			float squareXPts = this.QRCodeRightBottomXPts - (pSizeInSquares-x)*squareSizePts;
        			float squareYPts = this.QRCodeRightBottomYPts - (pSizeInSquares-y)*squareSizePts;
        			svg = svg+svgServices.generateRect(squareXPts, squareYPts, squareSizePts, squareSizePts, "none", this.QRCodeColor);
        			
        			//g.drawImage(bufferedFesti, x*FESTI_WIDTH, y*FESTI_WIDTH, null);
      		  	}
      	  	}
        }
    	
        return svg;
	}
}
