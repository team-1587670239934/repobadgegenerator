/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package importCsv.members;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import dataModel.IDataModelServices;

/**
 *
 * @author T0044185
 */
public abstract class AbsractMemberCsvReader 
{
    final private String fileName;
    final protected IDataModelServices modelServices;
    final protected ArrayList<MealField> mealFields;

    public AbsractMemberCsvReader(String pFileName, IDataModelServices pModelDataServices) {
        this.fileName = pFileName;
        this.modelServices = pModelDataServices;
        this.mealFields = new ArrayList<MealField>();
    }
    
    private boolean isStartOfSplitLine(String pLine)
    {
    	String[] splitParts = pLine.split(";");
    	int nbParts = splitParts.length;
    	String lastPart = splitParts[nbParts-1];
    	//la ligne est eun debut de split line si le dernier motif commence par " sans termin� par "
    	return lastPart.startsWith("\"") && !lastPart.endsWith("\"");
    }
    
    private boolean isEndOfSplitLine(String pLine)
    {
    	final boolean res;
    	String[] splitParts = pLine.split(";");
    	int nbParts = splitParts.length;
    	String firstPart = splitParts[0];
    	//la ligne est une fin de split line si le premier motif finit par "
    	return firstPart.endsWith("\"");
    }
    
    public void importMealFields(String headerLine)
    {
    	String[] headerFields = headerLine.split(";");
    	for (int headerIndex=0; headerIndex<headerFields.length; headerIndex++)
    	{
    		final String meal;
    		if (headerFields[headerIndex].contains("Dej"))
    		{
    			meal = "DEJ";
    		}
    		else if (headerFields[headerIndex].contains("Din"))
    		{
    			meal = "DIN";
    		}
    		else
    		{
    			meal = "";
    		}
    		
    		final String day;
    		if (headerFields[headerIndex].contains("Jeudi"))
    		{
    			day="JEU";
    		}
    		else if (headerFields[headerIndex].contains("Vendredi"))
    		{
    			day="VEN";
    		}
    		else if (headerFields[headerIndex].contains("Samedi"))
    		{
    			day="SAM";
    		}
    		else if (headerFields[headerIndex].contains("Dimanche"))
    		{
    			day="DIM";
    		}
    		else
    		{
    			day = "";
    		}
    		if (!meal.equals("") && !day.equals(""))
    		{
    			System.out.println("Ajout du "+meal+" du "+day);
    			this.mealFields.add(new MealField(headerIndex, headerFields[headerIndex], day+" "+meal));
    		}
    	}
    }
    
    public void importData() throws IOException
    {
        //FileReader fr=new FileReader(this.fileName);
    	InputStream flux= new FileInputStream(this.fileName);
        InputStreamReader lecture= new InputStreamReader(flux, StandardCharsets.UTF_8);
        BufferedReader br=new BufferedReader(lecture);    

        //lecture de la ligne d'entete
        String headerLine = br.readLine();
        this.importMealFields(headerLine);
        
        String currentLine = br.readLine();
        //currrentExportedLine contient les donn�es pr�c�demment lues au titre de la multiline en cours.
        //elle est en cours de construction
        String currentExportedLine = "";
        boolean isBetweenSplitLines = false;
        while(currentLine!=null)
        {    
        	if (isBetweenSplitLines)
        	{
        		//on est dans une split line, donc on ajoute forc�ment la currentLine � la currentExportedLine
        		//on remplace les \n par des //
        		currentExportedLine = currentExportedLine+"//"+currentLine;
        	}
        	else
        	{
        		//la ligne pr�c�dente n'�tait pas une split line
        		//on export la current exported line (sauf si elle est vide) et on repart sur une nouvelle exportedline
        		//on repart sur une nouvelle exported line, apr�s avoir exporter la current exported line
        		if (!currentExportedLine.equals(""))
        		{
        			this.importMember(currentExportedLine);
        		}
        		currentExportedLine = currentLine;
        	}
        	//nouveau status de isBetweenSplitLines
        	//on est au milieu d'une split line si :
        	//-on �tait dans une split line et qu'il n'y a pas de fin
        	//- on �tait dans une split line et il y a une fin et un nouveau debut
        	//- on �tait pas dans une split line et il y a un d�but
        	isBetweenSplitLines = 
        			(isBetweenSplitLines && !this.isEndOfSplitLine(currentLine))
        					|| (isBetweenSplitLines && this.isEndOfSplitLine(currentLine) && this.isStartOfSplitLine(currentLine))
        					|| (!isBetweenSplitLines && this.isStartOfSplitLine(currentLine));
        	//lecture de la prochaine ligne;
            currentLine=br.readLine();
        }
        
        //lorsque que la derniere ligne a �t� lue, il faut importer le contenu de la derni�re currentExportedLine
        this.importMember(currentExportedLine);
   
       
        br.close();    
        lecture.close();       
        
    }
    
    abstract String getTeamName(String pExtractedLine) throws IOException;
    
    abstract String getLastName(String pExtractedLine);
    
    abstract String getFirstName(String pExtractedLine);

    abstract ArrayList<String> getLangues(String pExtractedLine);
    
    abstract boolean getIsVege(String pExtractedLine);
    
    abstract boolean getIsLSF(String pExtractedLine);
    
    final String getId(String pExtractedLine)
    {
    	String[] fields = pExtractedLine.split(";");
        String id = fields[0].replace("\"", "");
        return id; 
    }
    
    final ArrayList<String> getMeals(String pExtractedLine)
    {
    	
    	ArrayList<String> res = new ArrayList<String>();
    	String[] fields = pExtractedLine.split(";");
    	for (MealField mealField:this.mealFields)
    	{
    		int mealFiedIndex = mealField.getFieldIndex();
    		String mealFieldValue = fields[mealFiedIndex];
    		if (mealFieldValue.replace("\"", "").equals("yes"))
    		{
    			res.add(mealField.getPrintedField());
    		}
    	}
    	return res;
    }
    
    
    private void importMember(String pExtractedLine) throws IOException
    {
        System.out.println(pExtractedLine);
        //String[] fields = pExtractedLine.split(";");
        String id = this.getId(pExtractedLine);
        String lastName = this.getLastName(pExtractedLine).trim();
        String firstName = this.getFirstName(pExtractedLine).trim();
        String teamName = this.getTeamName(pExtractedLine);
        boolean isVege = this.getIsVege(pExtractedLine);
        boolean isLSF = this.getIsLSF(pExtractedLine);
        ArrayList<String> meals = this.getMeals(pExtractedLine);
        
        ArrayList<String> alLangues = this.getLangues(pExtractedLine);
        
         this.modelServices.addMemberToTeam(id, firstName.toUpperCase(), lastName.toUpperCase(), meals, alLangues, teamName, isVege, isLSF);
    }
    
}
