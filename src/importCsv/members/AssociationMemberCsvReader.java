/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package importCsv.members;

import dataModel.IDataModelServices;
import java.util.ArrayList;

/**
 *
 * @author T0044185
 */
public class AssociationMemberCsvReader extends AbsractMemberCsvReader
{
	private final static int ASSO_FIELD_INDEX = 6;
	private final static int LAST_NAME_FIELD_INDEX = 2;
	private final static int FIRST_NAME_FIELD_INDEX = 1;
	private final static int LSF_FIELD_INDEX = 7;
	private final static int LANGUAGES_FIELD_INDEX = 15;
	private final static int VEGE_FIELD_INDEX = 13;
	
    public AssociationMemberCsvReader(String pFileName, IDataModelServices pModelDataServices) 
    {
        super(pFileName, pModelDataServices);
    }
    
    @Override
    String getTeamName(String pExtractedLine)
    {
        String[] fields = pExtractedLine.split(";");
        String team = fields[ASSO_FIELD_INDEX].replace("\"", "");
        if (team.equals(""))
        {
            team=this.modelServices.getDefaultAssociationName();
        }
        return team;
    }

    @Override
    String getLastName(String pExtractedLine) 
    {
        String[] fields = pExtractedLine.split(";");
        String lastName = fields[LAST_NAME_FIELD_INDEX].replace("\"", "");
        return lastName;
        
    }

    @Override
    String getFirstName(String pExtractedLine) 
    {
        String[] fields = pExtractedLine.split(";");
        String firstName = fields[FIRST_NAME_FIELD_INDEX].replace("\"", "");
        return firstName;
    }

    @Override
    ArrayList<String> getLangues(String pExtractedLine) 
    {
        String[] fields = pExtractedLine.split(";");
        ArrayList<String> alLangues = new ArrayList<String>();
        if (!fields[LANGUAGES_FIELD_INDEX].isEmpty())
        {
            String[] tabLangues = fields[LANGUAGES_FIELD_INDEX].replace("\"", "").split("//");
            for (String langue:tabLangues)
            {
                alLangues.add(langue);
            }
        }
        return alLangues;
    }

	@Override
	boolean getIsVege(String pExtractedLine) 
	{
		String[] fields = pExtractedLine.split(";");
        boolean isVege = fields[VEGE_FIELD_INDEX].replace("\"", "").equals("yes");
        return isVege;
	}

	@Override
	boolean getIsLSF(String pExtractedLine) 
	{
		String[] fields = pExtractedLine.split(";");
        boolean isLSF = fields[LSF_FIELD_INDEX].replace("\"", "").equals("yes");
        return isLSF;
	}
}
