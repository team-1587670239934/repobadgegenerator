package importCsv.members;

public class MealField 
{
	private int fieldIndex;
	private String fieldHeader;
	//champs affich� sur le badge
	private String printedField;
	
	public MealField(int pFieldIndex, String pFieldHeader, String pPrintedField)
	{
		this.fieldHeader = pFieldHeader;
		this.fieldIndex = pFieldIndex;
		this.printedField = pPrintedField;
	}

	public int getFieldIndex()
	{
		return this.fieldIndex;
	}
	
	public String getPrintedField()
	{
		return new String(this.printedField);
	}
}
