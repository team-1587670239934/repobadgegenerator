/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package importCsv.members;

import java.io.IOException;
import java.util.ArrayList;

import dataModel.IDataModelServices;
import exportPdf.StringNormalizer;

/**
 *
 * @author T0044185
 */
public class StructureMemberCsvReader extends AbsractMemberCsvReader
{
	private final static int ID_FIELD_INDEX = 0;
	private final static int FIRST_NAME_FIELD_INDEX = 1;
	private final static int LAST_NAME_FIELD_INDEX = 2;
	private final static int VEGE_FIELD_INDEX = 3;
	private final static int STRUCT_NAME_FIELD_INDEX = 9;

	
    public StructureMemberCsvReader(String pFileName, IDataModelServices pModelDataServices) 
    {
        super(pFileName, pModelDataServices);
    }
    
    @Override
    String getTeamName(String pExtractedLine) throws IOException
    {  	
    	final String teamName;
        String[] fields = pExtractedLine.split(";");
        String structName = fields[STRUCT_NAME_FIELD_INDEX].replace("\"", "");
        StringNormalizer normalizer = new StringNormalizer();
        teamName=normalizer.normalize(structName);
        return teamName;
    }

    @Override
    String getLastName(String pExtractedLine) 
    {
        String[] fields = pExtractedLine.split(";");
        String lastName = fields[LAST_NAME_FIELD_INDEX].replace("\"", "");
        return lastName;
        
    }

    @Override
    String getFirstName(String pExtractedLine) 
    {
        String[] fields = pExtractedLine.split(";");
        String firstName = fields[FIRST_NAME_FIELD_INDEX].replace("\"", "");
        return firstName;
    }

    @Override
    ArrayList<String> getLangues(String pExtractedLine) 
    {
        return new ArrayList<String>();
    }

	@Override
	boolean getIsVege(String pExtractedLine) 
	{
		String[] fields = pExtractedLine.split(";");
        String isVege = fields[VEGE_FIELD_INDEX].replace("\"", "");
        return isVege.equals("yes");
	}

	@Override
	boolean getIsLSF(String pExtractedLine) {
		// TODO Auto-generated method stub
		return false;
	}

	
}
