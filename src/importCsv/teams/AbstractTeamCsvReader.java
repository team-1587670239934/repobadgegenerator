/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package importCsv.teams;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import dataModel.IDataModelServices;

/**
 *
 * @author T0044185
 */
public abstract class AbstractTeamCsvReader 
{
    final private String fileName;
    final protected IDataModelServices modelServices;

    public AbstractTeamCsvReader(String pFileName, IDataModelServices pModelDataServices) {
        this.fileName = pFileName;
        this.modelServices = pModelDataServices;
    }
    
    public void importData() throws IOException
    {
        
            //FileReader fr=new FileReader(this.fileName);
            InputStream flux= new FileInputStream(this.fileName);
            InputStreamReader lecture= new InputStreamReader(flux, StandardCharsets.UTF_8);
            BufferedReader br=new BufferedReader(lecture);    

            int i;    
            String headerLine = br.readLine();
            String nextLine = br.readLine();
            while(nextLine!=null)
            {    
                this.importTeam(nextLine);
                nextLine=br.readLine();
            }  
            br.close();    
            lecture.close();       
    }
    
    abstract void importTeam(String pString) throws IOException;
    
    public abstract ArrayList<String> getImportedTeamNames();
}
