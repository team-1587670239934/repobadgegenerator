/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package importCsv.teams;

import java.io.IOException;
import java.util.ArrayList;

import dataModel.IDataModelServices;
import exportPdf.StringNormalizer;

/**
 *
 * @author T0044185
 */
public class StructureCsvReader extends AbstractTeamCsvReader
{
    public StructureCsvReader(String pFileName, IDataModelServices pModelDataServices) 
    {
        super(pFileName, pModelDataServices);
    }

    @Override
    void importTeam(String pExtractedString) throws IOException
    {
    	StringNormalizer normalizer = new StringNormalizer();
    	
        System.out.println(pExtractedString);
        
        String[] fields = pExtractedString.split(";");
        String teamName = normalizer.normalize(fields[0].replace("\"", ""));
        String teamLogo = fields[1].replace("\"", "");
       
        this.modelServices.addStructure(teamName, teamLogo);
    }

    @Override
    public ArrayList<String> getImportedTeamNames() 
    {
        return this.modelServices.getStructureNames();
    }
}
